﻿namespace Cambios
{
    using Cambios.Modelos;
    using Cambios.Servicos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Threading.Tasks;
    using System;

    public partial class Form1 : Form
    {
        #region atributo
        private List<Rates> rates;
        private NetworkService networkService;
        private ApiServices apiService;
        private DialogService dialogService;
        private DataService dataService;
        #endregion

        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiServices();
            dialogService = new DialogService();
            dataService = new DataService();
            LoadRates();
        }

        private async void LoadRates()
        {
            bool Load;

            LabelResultado.Text = "A Atualizar taxas...";
            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalRates();
                Load = false;
            }
            else
            {
                await LoadApiRates();
                Load = true;
            }
            if(rates.Count == 0)
            {
                LabelResultado.Text = "Não há ligação a Internet" + Environment.NewLine +
                    "e não foram préviamente carregadas as taxas." + Environment.NewLine +
                    "tente mas tarde";

                labelStatus.Text = "Primeira inicialização deverá ter ligãção a internet";
                return;
            }

            ComboBoxOrigem.DataSource = rates;
            ComboBoxOrigem.DisplayMember = "Name";

            //corrige bug da microsoft
            ComboBoxDestino.BindingContext = new BindingContext();

            ComboBoxDestino.DataSource = rates;
            ComboBoxDestino.DisplayMember = "Name";

            
            LabelResultado.Text = "Taxas Atualizadas...";

            ProgressBar1.Value = 100;
            ButtonConverter.Enabled = true;
            ButtonTroca.Enabled = true;

            if (Load)
            {
                labelStatus.Text = string.Format("Taxas carregadas da internet em {0:F}", DateTime.Now);
            }
            else
            {
                labelStatus.Text = string.Format("Taxas carregadas da Base de Dados.");
            }
        }

        private void LoadLocalRates()
        {
            rates = dataService.GetData();
        }

        private async Task LoadApiRates()
        {
            ProgressBar1.Value = 0;

            var response = await apiService.GetRates("http://apiexchangerates.azurewebsites.net", "/api/Rates");

            rates = (List<Rates>)response.Result;

            dataService.SaveData(rates);
        }

        private void ButtonConverter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if(string.IsNullOrEmpty(TextBoxValor.Text))
            {
                dialogService.ShorMessage("Erro", "Insira um valor a converter");
                return;
            }

            decimal valor;
            if(!decimal.TryParse(TextBoxValor.Text, out valor))
            {
                dialogService.ShorMessage("Erro de Conversão", "Valor terá que ser númerico");
                return;
            }
            if (ComboBoxOrigem.SelectedItem == null)
            {
                dialogService.ShorMessage("Erro", "Tem que escolher uma moeda a converter");
                return;
            }
            if (ComboBoxDestino.SelectedItem == null)
            {
                dialogService.ShorMessage("Erro", "Tem que escolher uma moeda de destino a comnverter");
                return;
            }
            var taxaOrigem = (Rates) ComboBoxOrigem.SelectedItem;
            var taxaDestino = (Rates) ComboBoxDestino.SelectedItem;

            var valorConvertido = valor / (decimal) taxaOrigem.TaxRate * (decimal) taxaDestino.TaxRate;

            LabelResultado.Text = string.Format("{0} {1:C2} = {2} {3:C2}", taxaOrigem.Code, valor, taxaDestino.Code, valorConvertido);
        }

        private void ButtonTroca_Click(object sender, EventArgs e)
        {
            Troca();
        }

        private void Troca()
        {
            var aux = ComboBoxOrigem.SelectedItem;
            ComboBoxOrigem.SelectedItem = ComboBoxDestino.SelectedItem;
            ComboBoxDestino.SelectedItem = aux;
            Converter();
        }
    }
}
