﻿namespace Cambios.Servicos
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Globalization;
    public class DataService
    {
        private SQLiteConnection connection;

        private SQLiteCommand command;

        private DialogService dialogService;

        public DataService()
        {
            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            var path = @"Data\Rates.sqlite";

            try
            {
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open();

                string sqlcommand = "create table if not exists rates(RateId int, Code varchar(5), TaxRate real, Name varchar(250))";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShorMessage("Erro", e.Message);
            }
        }

        public void SaveData(List<Rates> rates)
        {
            CultureInfo ci = new CultureInfo("en-us");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            try
            {
                foreach (var rate in rates)
                {
                    string sql = 
                        string.Format("insert into Rates (RateId, Code, TaxRate, Name) values({0}, '{1}', {2},'{3}')",
                        rate.RateId, rate.Code, rate.TaxRate, rate.Name);

                    command = new SQLiteCommand(sql, connection);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            catch (Exception e)
            {
                dialogService.ShorMessage("Erro",e.Message);
            }
        }

        public List<Rates> GetData()
        {
            List<Rates> rate = new List<Rates>();
            try
            {
                string sql = "select RateId, Code, TaxRate, Name from Rates";
                command = new SQLiteCommand(sql, connection);

                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    {
                        rate.Add(new Rates
                        {
                            RateId = (int)reader["RateId"],
                            Code = (string)reader["Code"],
                            TaxRate = (double)reader["TaxRate"],
                            Name = (string)reader["Name"]
                        });
                    }
                connection.Close();
                return rate;
            }
            catch (Exception e)
            {
                dialogService.ShorMessage("Erro", e.Message);
                return null;
            }
        }
        public void DeleteData()
        {
            try
            {
                string sql = "delete from Rates";
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShorMessage("Erro", e.Message);
            }
        }
    }
}
